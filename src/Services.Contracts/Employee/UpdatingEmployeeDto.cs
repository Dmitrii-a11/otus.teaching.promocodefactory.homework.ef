﻿namespace Services.Contracts.Customer
{
    public class UpdatingEmployeeDto
    {
        /// <summary>
        /// Возвращает, задает имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает фамилию
        /// </summary>
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает полное имя
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Возвращает, задает почту
        /// </summary>
        public string Email { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает кол-во примененных промокодов
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}
