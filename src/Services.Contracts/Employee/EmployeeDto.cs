﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО сотрудника
    /// </summary>
    public class EmployeeDto
    {
        /// <summary>
        /// Возвращает, задает имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает фамилию
        /// </summary>
        public string LastName { get; set; } = string.Empty;
    }
}
