﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО промокода
    /// </summary>
    public class PromoCodeDto
    {
        /// <summary>
        /// Возвращает, задает код
        /// </summary>
        public string Code { get; set; } = string.Empty;
    }
}
