﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО промокода для фильтрации
    /// </summary>
    public class PromoCodeFilterDto
    {
        /// <summary>
        /// Возвращает, задает код
        /// </summary>
        public string Code { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает информации об обслуживании
        /// </summary>
        public string ServiceInfo { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает дату начала действия промокода
        /// </summary>
        public DateTime BeginDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Возвращает, задает дату окончания действия промокода
        /// </summary>
        public DateTime EndDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Возвращает, задает имя партнера
        /// </summary>
        public string PartnerName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает кол-во элементов на страницу
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Возвращает, задает номер страницы
        /// </summary>
        public int Page { get; set; }
    }
}
