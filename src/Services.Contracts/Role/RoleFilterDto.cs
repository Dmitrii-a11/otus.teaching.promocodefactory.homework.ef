﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО роли для фильтрации
    /// </summary>
    public class RoleFilterDto
    {
        /// <summary>
        /// Возвращает, задает название роли
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает описание
        /// </summary>
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает кол-во элементов на страницу
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Возвращает, задает номер страницы
        /// </summary>
        public int Page { get; set; }
    }
}
