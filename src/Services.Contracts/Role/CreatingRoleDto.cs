﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО роли для создания
    /// </summary>
    public class CreatingRoleDto
    {
        /// <summary>
        /// Возвращает, задает название роли
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает описание
        /// </summary>
        public string Description { get; set; } = string.Empty;
    }
}
