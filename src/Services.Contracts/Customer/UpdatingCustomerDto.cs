﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО покупателя для обновления данных
    /// </summary>
    public class UpdatingCustomerDto
    {
        /// <summary>
        /// Возвращает, задает имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает фамилию
        /// </summary>
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает полное имя
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Возвращает, задает почту
        /// </summary>
        public string Email { get; set; } = string.Empty;

        public List<CustomerPreferenceDto> Preferencies { get; set; } = new();
    }
}
