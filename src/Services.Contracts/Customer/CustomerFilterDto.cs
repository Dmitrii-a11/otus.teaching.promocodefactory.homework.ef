﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО покупателя для фильтрации
    /// </summary>
    public class CustomerFilterDto
    {
        /// <summary>
        /// Возвращает, задает имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает фамилию
        /// </summary>
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает полное имя
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Возвращает, задает почту
        /// </summary>
        public string Email { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает кол-во элементов на страницу
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Возвращает, задает номер страницы
        /// </summary>
        public int Page { get; set; }
    }
}
