﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО покупателя для создания
    /// </summary>
    public class CreatingCustomerDto
    {
        /// <summary>
        /// Возвращает, задает имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает фамилию
        /// </summary>
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает полное имя
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Возвращает, задает почту
        /// </summary>
        public string Email { get; set; } = string.Empty;

        public List<Guid> PreferenceIds { get; set; } = new();
    }
}
