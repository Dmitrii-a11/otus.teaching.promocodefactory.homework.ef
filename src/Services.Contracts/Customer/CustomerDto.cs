﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО покупателя
    /// </summary>
    public class CustomerDto
    {
        /// <summary>
        /// Возвращает, задает имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает фамилию
        /// </summary>
        public string LastName { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string Id { get; set; } = string.Empty;

        public List<CustomerPreferenceDto> Preferencies { get; set; } = new();
    }
}
