﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО предпочтения для фильтрации
    /// </summary>
    public class PreferenceFilterDto
    {
        /// <summary>
        /// Возвращает, задает название предпочтения
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Возвращает, задает кол-во элементов на страницу
        /// </summary>
        public int ItemsPerPage { get; set; }

        /// <summary>
        /// Возвращает, задает номер страницы
        /// </summary>
        public int Page { get; set; }
    }
}
