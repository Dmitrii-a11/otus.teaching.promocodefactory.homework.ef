﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО предпочтения для обновления
    /// </summary>
    public class UpdatingPreferenceDto
    {
        /// <summary>
        /// Возвращает, задает название предпочтения
        /// </summary>
        public string Name { get; set; } = string.Empty;
    }
}
