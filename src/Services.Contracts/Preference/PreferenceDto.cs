﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО предпочтения
    /// </summary>
    public class PreferenceDto
    {
        /// <summary>
        /// Возвращает, задает название предпочтения
        /// </summary>
        public string Name { get; set; } = string.Empty;
    }
}
