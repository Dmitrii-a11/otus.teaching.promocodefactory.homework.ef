﻿namespace Services.Contracts.Customer
{
    /// <summary>
    /// Класс ДТО предпочтения для создания
    /// </summary>
    public class CreatingPreferenceDto
    {
        /// <summary>
        /// Возвращает, задает название предпочтения
        /// </summary>
        public string Name { get; set; } = string.Empty;
    }
}
