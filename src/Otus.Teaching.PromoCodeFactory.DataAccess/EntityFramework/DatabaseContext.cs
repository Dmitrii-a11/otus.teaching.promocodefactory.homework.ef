﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    public class DatabaseContext : DbContext
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="options">Настройки контекста базы данных (см. <see cref="DbContextOptions"/>)</param>
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        { }

        /// <summary>
        /// Возвращает, задает сотрудников
        /// </summary>
        public DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Возвращает, задает роли
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Возвращает, задает покупателей
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Возвращает, задает предпочтения
        /// </summary>
        public DbSet<Preference> Preferences { get; set; }

        /// <summary>
        /// Возвращает, задает промокоды
        /// </summary>
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Promocodes)
                .WithOne(p => p.Customer)
                .IsRequired();

            modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(100);

            modelBuilder.Entity<Role>().Property(c => c.Name).HasMaxLength(100);
            modelBuilder.Entity<Role>().Property(c => c.Description).HasMaxLength(100);

            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);

            modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(100);

            modelBuilder.Entity<PromoCode>().Property(c=>c.PartnerName).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(c=>c.ServiceInfo).HasMaxLength(100);
        }
    }
}
