﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Класс репозитория покупателей
    /// </summary>
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public CustomerRepository(DbContext context) : base(context)
        { }

        public CustomerRepository(DbContext context, IEnumerable<Customer> customers) : base(context)
        {
            Context.AddRange(customers);
            Context.SaveChanges();
        }

        /// <summary>
        /// Возвращает список покупателей
        /// </summary>
        /// <param name="page"> Номер страницы</param>
        /// <param name="itemsPerPage"> Количество элементов на странице</param>
        /// <returns> Список покупателей </returns>
        public Task<List<Customer>> GetPagedAsync(int page, int itemsPerPage)
        {
            return GetAll()
               .Skip((page - 1) * itemsPerPage)
               .Take(itemsPerPage)
               .ToListAsync();
        }
    }
}
