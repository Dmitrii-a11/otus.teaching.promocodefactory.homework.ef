﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Класс репозитория предпочтений
    /// </summary>
    public class PrefernceRepository : EfRepository<Preference>, IPreferenceRepository
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public PrefernceRepository(DbContext context) : base(context)
        { }

        public PrefernceRepository(DbContext context, IEnumerable<Preference> preferencies) : base(context)
        {
            Context.AddRange(preferencies);
            Context.SaveChanges();
        }

        /// <summary>
        /// Возвращает список предпочтений
        /// </summary>
        /// <param name="page"> Номер страницы</param>
        /// <param name="itemsPerPage"> Количество элементов на странице</param>
        /// <returns> Список предпочтений </returns>
        public Task<List<Preference>> GetPagedAsync(int page, int itemsPerPage)
        {
            return GetAll()
               .Skip((page - 1) * itemsPerPage)
               .Take(itemsPerPage)
               .ToListAsync();
        }
    }
}
