﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Абстрактный класс репозитория для Entity Framework
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public abstract class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DbContext Context;
        private readonly DbSet<T> _entitySet;

        protected EfRepository(DbContext context)
        {
            Context = context;
            _entitySet = Context.Set<T>();
        }

        /// <summary>
        /// Получить все сущности из базы данных
        /// </summary>
        /// <param name="asNoTracking">Нужно ли получать все сущности, как неотслеживаемые</param>
        /// <returns>Коллекция сущностей в виде <see cref="IQueryable{T}"/></returns>
        public virtual IQueryable<T> GetAll(bool asNoTracking = false)
        {
            return asNoTracking ? _entitySet.AsNoTracking() : _entitySet;
        }

        /// <summary>
        /// Получить все сущности асинхронно из базы данных
        /// </summary>
        /// <returns>Коллекция сущностей</returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await GetAll().ToListAsync();
        }

        /// <summary>
        /// Получить сущность из базы даннх асинхронно по id
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>Найденная сущность. Null, если сущность не была найдена</returns>
        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _entitySet.FindAsync(id);
        }

        /// <summary>
        /// Добавляет сущность в базу данных
        /// </summary>
        /// <param name="entity">Добавляемая сущность</param>
        /// <returns>Добавленная сущность</returns>
        public T Add(T entity)
        {
            return _entitySet.Add(entity).Entity;
        }

        /// <summary>
        /// Добавляет сущность в базу данных асинхронно
        /// </summary>
        /// <param name="entity">Добавляемая сущность</param>
        /// <returns>Добавленная сущность</returns>
        public async Task<T> AddAsync(T entity)
        {
            return (await _entitySet.AddAsync(entity)).Entity;
        }

        /// <summary>
        /// Обновляет сущность в базе данных
        /// </summary>
        /// <param name="entity">Добавляемая сущность</param>
        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Удаляет сущность из базы данных
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>True, если сущность была удалена</returns>
        public bool Delete(Guid id)
        {
            var obj = _entitySet.Find(id);
            if (obj is null)
                return false;

            _ = _entitySet.Remove(obj);

            return true;
        }

        /// <summary>
        /// Удаляет сущность из базы данных
        /// </summary>
        /// <param name="entity">Удаляемая сущность</param>
        /// <returns>True, если сущность была удалена</returns>
        public bool Delete(T entity)
        {
            if (entity is null)
                return false;

            _entitySet.Entry(entity).State = EntityState.Deleted;
            return true;
        }

        /// <summary>
        /// Удаляет сущности из базы данных
        /// </summary>
        /// <param name="entities">Удаляемые сущности</param>
        /// <returns>True, если сущности были удалены</returns>
        public bool DeleteRange(ICollection<T> entities)
        {
            if (entities == null || !entities.Any())
            {
                return false;
            }
            _entitySet.RemoveRange(entities);
            return true;
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await Context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
