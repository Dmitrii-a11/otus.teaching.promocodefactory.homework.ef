﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Класс репозитория сотрудников
    /// </summary>
    public class EmployeeRepository : EfRepository<Employee>, IEmployeeRepository
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public EmployeeRepository(DbContext context) : base(context)
        { }

        public EmployeeRepository(DbContext context, IEnumerable<Employee> employees) : base(context)
        {
            Context.AddRange(employees);
            Context.SaveChanges();
        }

        /// <summary>
        /// Возвращает список работников
        /// </summary>
        /// <param name="page"> Номер страницы</param>
        /// <param name="itemsPerPage"> Количество элементов на странице</param>
        /// <returns> Список работников </returns>
        public Task<List<Employee>> GetPagedAsync(int page, int itemsPerPage)
        {
            return GetAll()
                .Skip((page - 1) * itemsPerPage)
                .Take(itemsPerPage)
                .ToListAsync();
        }
    }
}
