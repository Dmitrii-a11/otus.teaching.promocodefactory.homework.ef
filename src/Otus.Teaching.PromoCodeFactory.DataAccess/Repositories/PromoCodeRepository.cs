﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Класс репозитория промокодов
    /// </summary>
    public class PromoCodeRepository : EfRepository<PromoCode>, IPromocodeRepository
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public PromoCodeRepository(DbContext context) : base(context)
        { }

        /// <summary>
        /// Возвращает список промокодов
        /// </summary>
        /// <param name="page"> Номер страницы</param>
        /// <param name="itemsPerPage"> Количество элементов на странице</param>
        /// <returns> Список промокодов </returns>
        public Task<List<PromoCode>> GetPagedAsync(int page, int itemsPerPage)
        {
            return GetAll()
               .Skip((page - 1) * itemsPerPage)
               .Take(itemsPerPage)
               .ToListAsync();
        }
    }
}
