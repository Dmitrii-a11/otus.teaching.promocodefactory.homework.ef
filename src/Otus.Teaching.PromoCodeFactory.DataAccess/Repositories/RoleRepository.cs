﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Класс репозитория ролей
    /// </summary>
    public class RoleRepository : EfRepository<Role>, IRoleRepository
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public RoleRepository(DbContext context) : base(context)
        { }

        public RoleRepository(DbContext context, IEnumerable<Role> roles) : base(context)
        {
            Context.AddRange(roles);
            Context.SaveChanges();
        }

        /// <summary>
        /// Возвращает список ролей
        /// </summary>
        /// <param name="page"> Номер страницы</param>
        /// <param name="itemsPerPage"> Количество элементов на странице</param>
        /// <returns> Список ролей </returns>
        public Task<List<Role>> GetPagedAsync(int page, int itemsPerPage)
        {
            return GetAll()
                .Skip((page - 1) * itemsPerPage)
                .Take(itemsPerPage)
                .ToListAsync();
        }
    }
}
