﻿using Services.Contracts.Customer;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса промокода
    /// </summary>
    public interface IPromoCodeService
    {
        /// <summary>
        /// Возвращает промокода
        /// </summary>
        /// <param name="id"> Id промокода</param>
        /// <returns> ДТО промокода</returns>
        Task<PromoCodeDto> GetByIdAsync(Guid id);

        /// <summary>
        /// Создает промокода
        /// </summary>
        /// <param name="creatingPromoCodeDto"> ДТО промокода</param>
        Task<Guid> CreateAsync(CreatingPromoCodeDto creatingPromoCodeDto);

        /// <summary>
        /// Изменяет промокода
        /// </summary>
        /// <param name="id"> Id промокода</param>
        /// <param name="updatingPromoCodeDto"> ДТО редактируемого промокода</param>
        Task UpdateAsync(Guid id, UpdatingPromoCodeDto updatingPromoCodeDto);

        /// <summary>
        /// Удаляет промокода
        /// </summary>
        /// <param name="id">Id промокода</param>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// Возвращает постраничный список
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра промокода</param>
        /// <returns> Список покупателей</returns>
        Task<ICollection<PromoCodeDto>> GetPagedAsync(PromoCodeFilterDto filterDto);
    }
}
