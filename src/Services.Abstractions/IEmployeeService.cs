﻿using Services.Contracts.Customer;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса сотрудника
    /// </summary>
    public interface IEmployeeService
    {
        /// <summary>
        /// Возвращает сотрудника
        /// </summary>
        /// <param name="id"> Id сотрудника</param>
        /// <returns> ДТО сотрудника</returns>
        Task<EmployeeDto> GetByIdAsync(Guid id);

        /// <summary>
        /// Создает сотрудника
        /// </summary>
        /// <param name="creatingEmployeeDto"> ДТО сотрудника</param>
        Task<Guid> CreateAsync(CreatingEmployeeDto creatingEmployeeDto);

        /// <summary>
        /// Изменяет сотрудника
        /// </summary>
        /// <param name="id"> Id сотрудника</param>
        /// <param name="updatingEmployeeDto"> ДТО редактируемого сотрудника</param>
        Task UpdateAsync(Guid id, UpdatingEmployeeDto updatingEmployeeDto);

        /// <summary>
        /// Удаляет сотрудника
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// Возвращает постраничный список
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра сотрудника</param>
        /// <returns> Список покупателей</returns>
        Task<ICollection<EmployeeDto>> GetPagedAsync(EmployeeFilterDto filterDto);
    }
}
