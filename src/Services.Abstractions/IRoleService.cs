﻿using Services.Contracts.Customer;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса роли
    /// </summary>
    public interface IRoleService
    {
        /// <summary>
        /// Возвращает роли
        /// </summary>
        /// <param name="id"> Id роли</param>
        /// <returns> ДТО роли</returns>
        Task<RoleDto> GetByIdAsync(Guid id);

        /// <summary>
        /// Создает роли
        /// </summary>
        /// <param name="creatingRoleDto"> ДТО роли</param>
        Task<Guid> CreateAsync(CreatingRoleDto creatingRoleDto);

        /// <summary>
        /// Изменяет роли
        /// </summary>
        /// <param name="id"> Id роли</param>
        /// <param name="updatingRoleDto"> ДТО редактируемого роли</param>
        Task UpdateAsync(Guid id, UpdatingRoleDto updatingRoleDto);

        /// <summary>
        /// Удаляет роли
        /// </summary>
        /// <param name="id">Id роли</param>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// Возвращает постраничный список
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра роли</param>
        /// <returns> Список покупателей</returns>
        Task<ICollection<RoleDto>> GetPagedAsync(RoleFilterDto filterDto);
    }
}
