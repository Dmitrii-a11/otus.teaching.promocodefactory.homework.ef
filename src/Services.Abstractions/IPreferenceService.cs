﻿using Services.Contracts.Customer;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса предпочтений
    /// </summary>
    public interface IPreferenceService
    {
        /// <summary>
        /// Возвращает предпочтений
        /// </summary>
        /// <param name="id"> Id предпочтений</param>
        /// <returns> ДТО предпочтений</returns>
        Task<PreferenceDto> GetByIdAsync(Guid id);

        /// <summary>
        /// Создает предпочтений
        /// </summary>
        /// <param name="creatingPreferenceDto"> ДТО предпочтений</param>
        Task<Guid> CreateAsync(CreatingPreferenceDto creatingPreferenceDto);

        /// <summary>
        /// Изменяет предпочтений
        /// </summary>
        /// <param name="id"> Id предпочтений</param>
        /// <param name="updatingPreferenceDto"> ДТО редактируемого предпочтений</param>
        Task UpdateAsync(Guid id, UpdatingPreferenceDto updatingPreferenceDto);

        /// <summary>
        /// Удаляет предпочтений
        /// </summary>
        /// <param name="id">Id предпочтений</param>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// Возвращает постраничный список
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра предпочтений</param>
        /// <returns> Список покупателей</returns>
        Task<ICollection<PreferenceDto>> GetPagedAsync(PreferenceFilterDto filterDto);
    }
}
