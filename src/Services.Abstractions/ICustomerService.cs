﻿using Services.Contracts.Customer;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса покупателя
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Возвращает покупателя
        /// </summary>
        /// <param name="id"> Id покупателя</param>
        /// <returns> ДТО покупателя</returns>
        Task<CustomerDto> GetByIdAsync(Guid id);

        /// <summary>
        /// Создает покупателя
        /// </summary>
        /// <param name="creatingCustomerDto"> ДТО покупателя</param>
        Task<Guid> CreateAsync(CreatingCustomerDto creatingCustomerDto);

        /// <summary>
        /// Изменяет покупателя
        /// </summary>
        /// <param name="id"> Id покупателя</param>
        /// <param name="updatingCustomerDto"> ДТО редактируемого покупателя</param>
        Task UpdateAsync(Guid id, UpdatingCustomerDto updatingCustomerDto);

        /// <summary>
        /// Удаляет покупателя
        /// </summary>
        /// <param name="id">Id покупателя</param>
        Task DeleteAsync(Guid id);

        /// <summary>
        /// Возвращает постраничный список
        /// </summary>
        /// <param name="filterDto"> ДТО фильтра покупателя</param>
        /// <returns> Список покупателей</returns>
        Task<ICollection<CustomerDto>> GetPagedAsync(CustomerFilterDto filterDto);
    }
}
