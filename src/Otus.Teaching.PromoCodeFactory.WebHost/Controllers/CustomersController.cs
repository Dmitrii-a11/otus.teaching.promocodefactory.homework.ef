﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Services.Abstractions;
using Services.Contracts.Customer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Возвращает всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var filterDto = new CustomerFilterDto
            {
                ItemsPerPage = 3,
                Page = 1
            };
            var customerDtos = await _customerService.GetPagedAsync(filterDto);
            var customers = new List<CustomerResponse>(customerDtos.Count);

            await Task.Run(() =>
            {
                foreach (var customerDto in customerDtos)
                {
                    customers.Add(new CustomerResponse
                    {
                        FirstName = customerDto.FirstName,
                        LastName = customerDto.LastName,
                        Email = customerDto.Email,
                        Id = Guid.Parse(customerDto.Id),
                    });

                    var lastCustomer = customers.Last();
                    foreach (var customerPreferenceDto in customerDto.Preferencies)
                    {
                        lastCustomer.Preferencies.Add(new Core.Domain.PromoCodeManagement.Preference
                        {
                            Id = Guid.Parse(customerPreferenceDto.Id),
                            Name = customerPreferenceDto.Name,
                        });
                    }
                }
            });

            return Ok(customers);
        }
        
        /// <summary>
        /// Возвращает клиента
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            CustomerDto customerDto = await _customerService.GetByIdAsync(id);
            CustomerResponse customer = new CustomerResponse
            {
                FirstName = customerDto.FirstName,
                LastName = customerDto.LastName,
                Email = customerDto.Email,
                Id = Guid.Parse(customerDto.Id)
            };

            foreach (var customerPreferenceDto in customerDto.Preferencies)
            {
                customer.Preferencies.Add(new Core.Domain.PromoCodeManagement.Preference
                {
                    Id = Guid.Parse(customerPreferenceDto.Id),
                    Name = customerPreferenceDto.Name
                });
            }

            return Ok(customer);
        }
        
        /// <summary>
        /// Создает клиента
        /// </summary>
        /// <param name="request">Клиент</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            List<CustomerPreferenceDto> customerPreferenceDtos = new(request.PreferenceIds.Count);
            var creatingCustomerDto = new CreatingCustomerDto
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                
            };

            await Task.Run(() =>
            {
                foreach (var preference in request.PreferenceIds)
                {
                    creatingCustomerDto.PreferenceIds.Add(preference);
                }
            });

            await _customerService.CreateAsync(creatingCustomerDto);
            return Ok();
        }
        
        /// <summary>
        /// Изменить данные клиента
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <param name="request">Данные для обновления клиента</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            UpdatingCustomerDto updatingCustomerDto = new()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };

            await _customerService.UpdateAsync(id, updatingCustomerDto);
            return Ok();
        }
        
        /// <summary>
        /// Удаляет клиента
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerService.DeleteAsync(id);
            return Ok();
        }
    }
}