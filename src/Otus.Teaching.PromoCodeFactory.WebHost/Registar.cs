﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using Services.Abstractions;
using Services.Implementations;
using Microsoft.EntityFrameworkCore;
using System.Runtime.CompilerServices;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    /// <summary>
    /// Класс регистраиции сервисов
    /// </summary>
    public static class Registar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();
            services.AddSingleton(applicationSettings)
                    .AddSingleton((IConfigurationRoot)configuration)
                    .ConfigureContext(applicationSettings.ConnectionString)
                    .InstallRepositories()
                    .InstallServices();
            return services;
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<ICustomerService, CustomerService>()
                .AddTransient<IEmployeeService, EmployeeService>()
                .AddTransient<IPreferenceService, PreferenceService>()
                .AddTransient<IPromoCodeService, PromoCodeService>()
                .AddTransient<IRoleService, RoleService>();
            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
           
            serviceCollection
                .AddTransient<ICustomerRepository, CustomerRepository>(x =>
                {
                    var context = x.GetRequiredService<DbContext>();
                    var repo = new CustomerRepository(context, DataAccess.Data.FakeDataFactory.Customers);
                    return repo;
                });
            _ = serviceCollection
                .AddTransient<IEmployeeRepository, EmployeeRepository>(x =>
                {
                    var context = x.GetRequiredService<DbContext>();
                    var repo = new EmployeeRepository(context, DataAccess.Data.FakeDataFactory.Employees);
                    return repo;
                });
            _ = serviceCollection
                .AddTransient<IPreferenceRepository, PrefernceRepository>(x =>
                {
                    var context = x.GetRequiredService<DbContext>();
                    var repo = new PrefernceRepository(context, DataAccess.Data.FakeDataFactory.Preferences);
                    return repo;
                });
            _ = serviceCollection.AddTransient<IPromocodeRepository, PromoCodeRepository>();
            _ = serviceCollection.AddTransient<IRoleRepository, RoleRepository>(x =>
            {
                var context = x.GetRequiredService<DbContext>();
                var repo = new RoleRepository(context, DataAccess.Data.FakeDataFactory.Roles);
                return repo;
            });
            return serviceCollection;
        }
    }
}
