﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Интерфейс репозитория предпочтения
    /// </summary>
    public interface IPreferenceRepository : IRepository<Preference>
    {
        /// <summary>
        /// Возвращает список предпочтений
        /// </summary>
        /// <param name="page"> Номер страницы</param>
        /// <param name="itemsPerPage"> Количество элементов на странице</param>
        /// <returns> Список предпочтений </returns>
        Task<List<Preference>> GetPagedAsync(int page, int itemsPerPage);
    }
}
