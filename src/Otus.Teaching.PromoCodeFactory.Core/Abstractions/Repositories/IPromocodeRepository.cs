﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Интерфейс репозитория промокода
    /// </summary>
    public interface IPromocodeRepository : IRepository<PromoCode>
    {
        /// <summary>
        /// Возвращает список промокодов
        /// </summary>
        /// <param name="page"> Номер страницы</param>
        /// <param name="itemsPerPage"> Количество элементов на странице</param>
        /// <returns> Список промокодов </returns>
        Task<List<PromoCode>> GetPagedAsync(int page, int itemsPerPage);
    }
}
