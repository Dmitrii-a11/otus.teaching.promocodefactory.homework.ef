﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T> : IDisposable
        where T: BaseEntity
    {
        /// <summary>
        /// Возвращает сущности асинхронно
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync();
        
        /// <summary>
        /// Возвращает сущность асинхронно
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>Сущность</returns>
        Task<T> GetByIdAsync(Guid id);

        T Add(T entity);

        /// <summary>
        /// Добавляет сущность в базу данных асинхронно
        /// </summary>
        /// <param name="entity">Добавляемая сущность</param>
        /// <returns>Добавленная сущность</returns>
        Task<T> AddAsync(T entity);

        /// <summary>
        /// Обновляет сущность в базе данных
        /// </summary>
        /// <param name="entity">Добавляемая сущность</param>
        void Update(T entity);

        /// <summary>
        /// Удаляет сущность из базы данных
        /// </summary>
        /// <param name="id">Id сущности</param>
        /// <returns>True, если сущность была удалена</returns>
        bool Delete(Guid id);

        /// <summary>
        /// Удаляет сущность из базы данных
        /// </summary>
        /// <param name="entity">Удаляемая сущность</param>
        /// <returns>True, если сущность была удалена</returns>
        bool Delete(T entity);

        /// <summary>
        /// Удаляет сущности из базы данных
        /// </summary>
        /// <param name="entities">Удаляемые сущности</param>
        /// <returns>True, если сущности были удалены</returns>
        bool DeleteRange(ICollection<T> entities);

        Task SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}