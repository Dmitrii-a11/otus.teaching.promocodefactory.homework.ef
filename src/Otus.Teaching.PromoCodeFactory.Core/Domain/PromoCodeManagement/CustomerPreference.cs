﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference : BaseEntity
    {
        public Guid CustomerId { get; set; }

        public Customer Customer { get; set; }

        public string Name { get; set; } = string.Empty;
    }
}
