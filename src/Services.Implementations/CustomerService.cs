﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Services.Abstractions;
using Services.Contracts.Customer;

namespace Services.Implementations
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IPreferenceRepository _preferenceRepository;

        public CustomerService(ICustomerRepository customerRepository,
                               IPreferenceRepository preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<Guid> CreateAsync(CreatingCustomerDto creatingCustomerDto)
        {
            var preferencies = await _preferenceRepository.GetAllAsync();

            var customer = new Customer
            {
                Email = creatingCustomerDto.Email,
                FirstName = creatingCustomerDto.FirstName,
                LastName = creatingCustomerDto.LastName,
                Id = Guid.NewGuid()
            };

            await Task.Run(() =>
            {
                foreach (var preference in preferencies)
                {
                    customer.Preferencies.Add(new CustomerPreference
                    {
                        Id = preference.Id,
                        Name = preference.Name,
                        Customer = customer
                    });
                }
            });

            var returnedObject = await _customerRepository.AddAsync(customer);

            await _customerRepository.SaveChangesAsync();

            return returnedObject.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            _ = _customerRepository.Delete(customer);
        }

        public async Task<CustomerDto> GetByIdAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id) ?? throw new InvalidOperationException($"Покупатель с id: {id} не найден");
            var customerDto = new CustomerDto
            {
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Id = id.ToString(),
                
            };

            await Task.Run(() =>
            {
                foreach (var customerPreference in customer.Preferencies)
                {
                    customerDto.Preferencies.Add(new CustomerPreferenceDto
                    {
                        Id = customerPreference.Id.ToString(),
                        Name = customerPreference.Name
                    });
                }
            });

            return customerDto;
        }

        public async Task<ICollection<CustomerDto>> GetPagedAsync(CustomerFilterDto filterDto)
        {
            var customers = await _customerRepository.GetPagedAsync(filterDto.Page, filterDto.ItemsPerPage);
            var customerDtos = new List<CustomerDto>(customers.Count);

            await Task.Run(() =>
            {
                for (int i = 0; i < customers.Count; i++)
                {
                    customerDtos.Add(new CustomerDto
                    {
                        FirstName = customers[i].FirstName,
                        LastName = customers[i].LastName,
                        Id = customers[i].Id.ToString(),
                        Email = customers[i].Email
                    });

                    var lastCustomerDto = customerDtos.Last();
                    foreach (var customerPrefernce in customers[i].Preferencies)
                    {
                        lastCustomerDto.Preferencies.Add(new CustomerPreferenceDto
                        {
                            Id = customerPrefernce.Id.ToString(),
                            Name = customerPrefernce.Name
                        });
                    }
                }
            });

            return customerDtos;
        }

        public async Task UpdateAsync(Guid id, UpdatingCustomerDto updatingCustomerDto)
        {
            var customer = await _customerRepository.GetByIdAsync(id) ?? throw new InvalidOperationException(GetCustomerNotFoundMessage(id));

            customer.Email = updatingCustomerDto.Email;
            customer.FirstName = updatingCustomerDto.FirstName;
            customer.LastName = updatingCustomerDto.LastName;

            await Task.Run(() =>
            {
                customer.Preferencies.Clear();
                foreach (var preference in updatingCustomerDto.Preferencies)
                {
                    customer.Preferencies.Add(new CustomerPreference
                    {
                        Id = Guid.Parse(preference.Id),
                        Name = preference.Name,
                        Customer = customer
                    });
                }
            });

            _customerRepository.Update(customer);
            await _customerRepository.SaveChangesAsync();
        }

        private static string GetCustomerNotFoundMessage(Guid id)
        {
            return $"Покупатель с id: {id} не найден";
        }
    }
}
