﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Services.Abstractions;
using Services.Contracts.Customer;

namespace Services.Implementations
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IPromocodeRepository _promocodeRepository;

        public PromoCodeService(IPromocodeRepository promocodeRepository)
        {
            _promocodeRepository = promocodeRepository;
        }

        public async Task<Guid> CreateAsync(CreatingPromoCodeDto creatingPromoCodeDto)
        {
            var promoCode = new PromoCode
            {
                BeginDate = creatingPromoCodeDto.BeginDate,
                Code = creatingPromoCodeDto.Code,
                EndDate = creatingPromoCodeDto.EndDate,
                PartnerName = creatingPromoCodeDto.PartnerName,
                ServiceInfo = creatingPromoCodeDto.ServiceInfo,
                Id = Guid.NewGuid()
            };
            var returnedObject = await _promocodeRepository.AddAsync(promoCode);

            await _promocodeRepository.SaveChangesAsync();

            return returnedObject.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var promoCode = await _promocodeRepository.GetByIdAsync(id);
            _ = _promocodeRepository.Delete(promoCode);
        }

        public async Task<PromoCodeDto> GetByIdAsync(Guid id)
        {
            var promoCode = await _promocodeRepository.GetByIdAsync(id);
            var promoCodeDto = new PromoCodeDto
            {
                Code = promoCode.Code
            };

            return promoCodeDto;
        }

        public async Task<ICollection<PromoCodeDto>> GetPagedAsync(PromoCodeFilterDto filterDto)
        {
            var promoCodes = await _promocodeRepository.GetPagedAsync(filterDto.Page, filterDto.ItemsPerPage);
            var preferenceDtos = new List<PromoCodeDto>(promoCodes.Count);

            await Task.Run(() =>
            {
                for (int i = 0; i < promoCodes.Count; i++)
                {
                    preferenceDtos.Add(new PromoCodeDto
                    {
                        Code = promoCodes[i].Code
                    });
                }
            });

            return preferenceDtos;
        }

        public async Task UpdateAsync(Guid id, UpdatingPromoCodeDto updatingPromoCodeDto)
        {
            var promoCode = await _promocodeRepository.GetByIdAsync(id);

            promoCode.Code = updatingPromoCodeDto.Code;
            promoCode.ServiceInfo = updatingPromoCodeDto.ServiceInfo;
            promoCode.PartnerName = updatingPromoCodeDto.PartnerName;
            promoCode.BeginDate = updatingPromoCodeDto.BeginDate;
            promoCode.EndDate = updatingPromoCodeDto.EndDate;

            await _promocodeRepository.SaveChangesAsync();
        }
    }
}
