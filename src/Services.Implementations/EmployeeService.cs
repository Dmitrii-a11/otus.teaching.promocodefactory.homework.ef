﻿using Services.Abstractions;
using Services.Contracts.Customer;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Services.Implementations
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _eployeeRepository;

        public EmployeeService(IEmployeeRepository eployeeRepository)
        {
            _eployeeRepository = eployeeRepository;
        }

        public async Task<Guid> CreateAsync(CreatingEmployeeDto creatingEmployeeDto)
        {
            var employee = new Employee
            {
                AppliedPromocodesCount = creatingEmployeeDto.AppliedPromocodesCount,
                Email = creatingEmployeeDto.Email,
                FirstName = creatingEmployeeDto.FirstName,
                LastName = creatingEmployeeDto.LastName,
                Id = Guid.NewGuid()
            };
            var returnedObject = await _eployeeRepository.AddAsync(employee);

            await _eployeeRepository.SaveChangesAsync();

            return returnedObject.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var employee = await _eployeeRepository.GetByIdAsync(id);
            _ = _eployeeRepository.Delete(employee);
        }

        public async Task<EmployeeDto> GetByIdAsync(Guid id)
        {
            var employee = await _eployeeRepository.GetByIdAsync(id);
            var employeeDto = new EmployeeDto
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName
            };

            return employeeDto;
        }

        public async Task<ICollection<EmployeeDto>> GetPagedAsync(EmployeeFilterDto filterDto)
        {
            var employees = await _eployeeRepository.GetPagedAsync(filterDto.Page, filterDto.ItemsPerPage);
            var employeeDtos = new List<EmployeeDto>(employees.Count);

            await Task.Run(() =>
            {
                for (int i = 0; i < employees.Count; i++)
                {
                    employeeDtos.Add(new EmployeeDto
                    {
                        FirstName = employees[i].FirstName,
                        LastName = employees[i].LastName
                    });
                }
            });

            return employeeDtos;
        }

        public async Task UpdateAsync(Guid id, UpdatingEmployeeDto updatingEmployeeDto)
        {
            var employee = await _eployeeRepository.GetByIdAsync(id);

            employee.Email = updatingEmployeeDto.Email;
            employee.FirstName = updatingEmployeeDto.FirstName;
            employee.LastName = updatingEmployeeDto.LastName;

            await _eployeeRepository.SaveChangesAsync();
        }
    }
}
