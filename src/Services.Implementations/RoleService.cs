﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Services.Abstractions;
using Services.Contracts.Customer;

namespace Services.Implementations
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<Guid> CreateAsync(CreatingRoleDto creatingRoleDto)
        {
            var role = new Role
            {
                Description = creatingRoleDto.Description,
                Name = creatingRoleDto.Name,
                Id = Guid.NewGuid()
            };
            var returnedObject = await _roleRepository.AddAsync(role);

            await _roleRepository.SaveChangesAsync();
            return returnedObject.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var role = await _roleRepository.GetByIdAsync(id);
            _ = _roleRepository.Delete(role);
        }

        public async Task<RoleDto> GetByIdAsync(Guid id)
        {
            var role = await _roleRepository.GetByIdAsync(id);
            var roleDto = new RoleDto
            {
                Description= role.Description,
                Name = role.Name
            };

            return roleDto;
        }

        public async Task<ICollection<RoleDto>> GetPagedAsync(RoleFilterDto filterDto)
        {
            var roles = await _roleRepository.GetPagedAsync(filterDto.Page, filterDto.ItemsPerPage);
            var roleDtos = new List<RoleDto>(roles.Count);

            await Task.Run(() =>
            {
                for (int i = 0; i < roles.Count; i++)
                {
                    roleDtos.Add(new RoleDto
                    {
                        Description = roles[i].Description,
                        Name = roles[i].Name
                    });
                }
            });

            return roleDtos;
        }

        public async Task UpdateAsync(Guid id, UpdatingRoleDto updatingRoleDto)
        {
            var role = await _roleRepository.GetByIdAsync(id);

            role.Name = updatingRoleDto.Name;
            role.Description = updatingRoleDto.Description;

            await _roleRepository.SaveChangesAsync();
        }
    }
}
