﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Services.Abstractions;
using Services.Contracts.Customer;

namespace Services.Implementations
{
    public class PreferenceService : IPreferenceService
    {
        private readonly IPreferenceRepository _preferenceRepository;

        public PreferenceService(IPreferenceRepository preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        public async Task<Guid> CreateAsync(CreatingPreferenceDto creatingPreferenceDto)
        {
            var preference = new Preference
            {
                Name = creatingPreferenceDto.Name,
                Id = Guid.NewGuid()
            };
            var returnedObject = await _preferenceRepository.AddAsync(preference);

            await _preferenceRepository.SaveChangesAsync();

            return returnedObject.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            _ = _preferenceRepository.Delete(preference);
        }

        public async Task<PreferenceDto> GetByIdAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            var preferenceDto = new PreferenceDto
            {
                Name = preference.Name
            };

            return preferenceDto;
        }

        public async Task<ICollection<PreferenceDto>> GetPagedAsync(PreferenceFilterDto filterDto)
        {
            var preferencies = await _preferenceRepository.GetPagedAsync(filterDto.Page, filterDto.ItemsPerPage);
            var preferenceDtos = new List<PreferenceDto>(preferencies.Count);

            await Task.Run(() =>
            {
                for (int i = 0; i < preferencies.Count; i++)
                {
                    preferenceDtos.Add(new PreferenceDto
                    {
                        Name = preferencies[i].Name
                    });
                }
            });

            return preferenceDtos;
        }

        public async Task UpdateAsync(Guid id, UpdatingPreferenceDto updatingPreferenceDto)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            preference.Name = updatingPreferenceDto.Name;

            await _preferenceRepository.SaveChangesAsync();
        }
    }
}
